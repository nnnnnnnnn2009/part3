﻿console.log('++main.js');

function modal_alert(message) {
    $("#modDialog h4").text(message);
    $("#modDialog").modal('show');
}

function click_slide(id) {
    var sel = "#" + id;
    $(sel).slideToggle(1000);
}

function cancel_slide(id) {
    var sel = "#" + id;
    $(sel).slideUp(1000);
    //alert($('html,body').scrollTop());
    //$('html,body').animate({ scrollTop: $('html,body').scrollTop() - 100 }, 500);
    window.scrollTo(0, 0);
}

console.log('--main.js');