﻿function validation_loginForm() {
    var form = $("#loginForm");
    var email = form.find("#Email");
    var password = form.find("#Password");
    var reg_password1 = /\d+/;
    var reg_password2 = /[a-z]+/i;
    var reg_email = /^([a-z0-9]+)(\.[a-z0-9]+)*@([a-z0-9]+)(\.[a-z0-9]+)+$/i;
    var valid = true;
    if (!reg_password1.test(password.val()) || !reg_password2.test(password.val())) {
        password.next().text("Пароль повинен містити цифру і літеру");
        password.closest(".form-group").addClass("has-error");
        valid = false;
    } else {
        password.next().text("");
        password.closest(".form-group").removeClass("has-error");
    }

    if (!reg_email.test(email.val())) {
        email.next().text("Це не подібне на e-mail");
        email.closest(".form-group").addClass("has-error");
        valid = false;
    } else {
        email.next().text("");
        email.closest(".form-group").removeClass("has-error");
    }
    return valid;
}
