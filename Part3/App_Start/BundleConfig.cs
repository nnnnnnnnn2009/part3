﻿using System.Web;
using System.Web.Optimization;

namespace Part3
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.2.3.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/App/content/style_angular.css"));

            bundles.Add(new ScriptBundle("~/bundles/angular-components").Include(
                      "~/App/libs/angular.js",
                      "~/App/libs/angular-messages.js",
                      "~/App/libs/angular-ui-router.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-factory").Include(
                    "~/App/factory/home.service.js",
                    "~/App/factory/myPosts.service.js",
                    "~/App/factory/allPosts.service.js",
                    "~/App/factory/message.service.js",
                    "~/App/factory/realtime.service.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-home").Include(
                    "~/App/home/homeController.js",
                    "~/App/home/app.home.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-my-posts").Include(
                    "~/App/myPosts/myPostsController.js",
                    "~/App/myPosts/app.myPosts.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-all-posts").Include(
                    "~/App/allPosts/allPostsController.js",
                    "~/App/allPosts/uploadingPosts.directive.js",
                    "~/App/allPosts/app.allPosts.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-message").Include(
                    "~/App/message/messageController.js",
                    "~/App/message/app.message.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-register").Include(
                    "~/App/register/registerController.js",
                    "~/App/register/app.register.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-base").Include(
                    "~/App/directive/toggleSlide.directive.js",
                    "~/App/header/headerController.js",
                    "~/App/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                    "~/Scripts/jquery.signalR-2.2.1.js"));

        }
    }
}
