﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using Part3;
using Part3.Models;

[assembly: OwinStartupAttribute(typeof(Part3.Startup))]
namespace Part3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var idProvider = new UserProvider();
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => idProvider);
            app.MapSignalR();
        }
    }
}
