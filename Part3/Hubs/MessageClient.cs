﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Part3.Models;
//using Microsoft.AspNet.Identity;

namespace Part3.Hubs
{
    public class MessageClient
    {
        public int PostId { get; set; }
        public string PostTitle { get; set; }
        public string UserName { get; set; }
        public bool? LikeType { get; set; }
        public bool? LikeTypeOld { get; set;  }
        public bool IsNew { get; set; }
    }

}

