﻿angular
    .module("app", ['ngMessages',
        'ui.router',
        'home.service',
        'myPosts.service',
        'allPosts.service',
        'realtime.service',
        'message.service',
        'app.home', 'app.myPosts', , 'app.allPosts', 'app.message' , 'app.register'])
    .config(['$urlRouterProvider', '$locationProvider', function ($urlRouterProvider, $locationProvider) {
        /*$locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });*/
        $urlRouterProvider.otherwise('/home');
    }])
    .controller("headerController", headerController)
    .directive('toggleSlide', toggleSlide)
    .run(['$rootScope', 'messageService', function ($rootScope, messageService) {
        $rootScope.myId = undefined;

        $rootScope.$on('$locationChangeSuccess', function (evt, pathNew, pathOld) {

            var namePathOld = pathOld.substr(pathOld.lastIndexOf('/') + 1);
            console.log('namePathOld: ', namePathOld);
            if (namePathOld == 'message') {
                messageService.countNewMessages = 0;
                var mess = messageService.messages
                var len = mess.length;
                for (var i = 0; i < len; i++)
                    mess[i].IsNew = false;
                messageService.messages = mess;

            }

        });
    }])

