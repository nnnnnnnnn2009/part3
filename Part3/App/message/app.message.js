﻿angular
    .module('app.message', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('message', {
            url: "/message",
            template: '<message-component></message-component>',
            data: {
                pageTitle: 'Сповіщення',
            }
        });
    }])
    .controller('messageController', messageController)
    .component('messageComponent', {
        templateUrl: '/App/message/message.html',
        controller: messageController,
        bindings: {
        }
    })

