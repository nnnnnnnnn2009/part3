﻿function headerController($scope, messageService, realtimeService, allPostsService) {
    console.log('++headerController');
    //var ctrl = this;
    $scope.messageService = messageService;
    //console.log('messageService: ', $scope.messageService);
    //$scope.count = 4;

    realtimeService.on('takeLike', takeLike);
    realtimeService.on('cleanLike', cleanLike);
    realtimeService.start();
    realtimeService.invoke('test');

    function takeLike(data) {

        console.log('in takelike');
        console.log('data: ', data);
        messageService.countNewMessages += 1;
        messageService.messages.unshift(data);
        //console.log('messageService.messages: ', messageService.messages);

        if (typeof (allPostsService.allPosts) != 'undefined') {
            var len = allPostsService.allPosts.length;
            var index = undefined;
            for (var i = 0; i < len; i++) {
                if (allPostsService.allPosts[i].Id == data.PostId) {
                    index = i;
                    break;
                }
            }
        }

        if (typeof (index) != 'undefined') {
            if (data.LikeType) 
                allPostsService.allPosts[index].CountLikesUp += 1;
            else
                allPostsService.allPosts[index].CountLikesDown += 1;
        }
    }
        

    function cleanLike(data) {

        console.log('in cleanLike');
        console.log('data: ', data);
        messageService.countNewMessages += 1;
        messageService.messages.unshift(data);
        //console.log('messageService.messages: ', messageService.messages);

        if (typeof (allPostsService.allPosts) != 'undefined') {
            var len = allPostsService.allPosts.length;
            var index = undefined;
            for (var i = 0; i < len; i++) {
                if (allPostsService.allPosts[i].Id == data.PostId) {
                    index = i;
                    break;
                }
            }
        }

        if (typeof (index) != 'undefined') {
            if (data.LikeTypeOld)
                allPostsService.allPosts[index].CountLikesUp -= 1;
            else
                allPostsService.allPosts[index].CountLikesDown -= 1;
        }
    }


}

headerController.$inject = ['$scope', 'messageService', 'realtimeService', 'allPostsService'];

