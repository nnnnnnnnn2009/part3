﻿angular
    .module('app.home', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('home', {
            url: "/home",
            template: '<home-component></home-component>',
            data: {
                pageTitle: 'Головна',
            }
        });
    }])
    .controller('homeController', homeController)
    .component('homeComponent', {
        templateUrl: '/App/Home/home.html',
        controller: homeController,
        bindings: {
        }
    })

