﻿function homeController(homeService) {
    console.log('++homeController');
    var vm = this;
    vm.isLoadingUsers = (typeof (homeService.users) == 'undefined') ? true : false;
    vm.homeService = homeService;

    if (typeof (homeService.users) == 'undefined') {
        homeService.gethomeUsers().then(function (data) {
            console.log('data.data: ', data.data);
            vm.isLoadingUsers = false;
            if (data.data.success !== true) {
                return;
            } else {
                homeService.users = data.data.users;
            }
        });
    } 

    vm.signed = function (idUser, index) {
        console.log('in signed');
        homeService.signed(idUser, index).then(function (data) {
            console.log('data signed: ', data);
            if (data.data.success === true) {
                var user = homeService.users[index];
                user.IsISigned = true;
                user.IdFriend = data.data.idFriend;
                homeService.users[index] = user;
            } else {
                modal_alert('Невідома помилка');
            }
        });
    }

    vm.outSigned = function (idFriend, index) {
        console.log('in outsigned');
        homeService.outSigned(idFriend, index).then(function (data) {
            console.log('data outSigned: ', data);
            if (data.data.success === true) {
                var user = homeService.users[index];
                user.IdFriend = null;
                user.IsISigned = false;
                homeService.users[index] = user;
            } else {
                modal_alert('Невідома помилка');
            }
        });
    }

    vm.makeFriends = function (idFriend, index) {
        console.log('in makeFriends');
        homeService.makeFriends(idFriend, index).then(function (data) {
            console.log('data makeFriends: ', data);
            if (data.data.success === true) 
                homeService.users[index].IsFriends = true;
            else
                modal_alert('Невідома помилка');
        });
    }

    vm.deleteFriend = function (idFriend, index) {
        console.log('in deleteFriends');
        homeService.deleteFriend(idFriend, index).then(function (data) {
            console.log('data deleteFriends: ', data);
            if (data.data.success === true) {
                homeService.users[index].IsFriends = false;
            } else {
                modal_alert('Невідома помилка');
            }
        });
    }

}

homeController.$inject = ['homeService'];