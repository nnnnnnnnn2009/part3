﻿function registerController($scope) {
    console.log('++registerController');
    //var vm = this;
    $scope.reg_password = /([a-z]+\d+)|(\d+[a-z]+)/i;
    $scope.reg_email = /^([a-z0-9]+)(\.[a-z0-9]+)*@([a-z0-9]+)(\.[a-z0-9]+)+$/i;

}

registerController.$inject = ['$scope'];