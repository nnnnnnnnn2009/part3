﻿function uploadingPosts(allPostsService, $document, $state) {

    return {
        restrict: 'E',

        link: function (scope, element, attrs) {
            console.log('$document.on(scroll): ', $document.on('scroll'));
            /*
            var raw = elm[0];
                elm.bind('scroll', function() {
                    if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                        scope.$apply(attr.whenScrolled);
                    }
                });
            */


            //console.log('directive uploadingPosts element: ', element);
            //console.log('scope: ', scope);
            //if (typeof (allPostsService.allPosts) == 'undefined') {

            if (typeof (allPostsService.allPosts) == 'undefined') {

                $document.on('scroll', function documentScroll() {
                    //console.log('$state: ', $state);
                    //console.log('$state.current.name: ', $state.current.name);

                    if ($state.current.name != 'all-posts')
                        return;

                    if (allPostsService.isAllPostsDownloading) {
                        $document.off('scroll', documentScroll);
                        return;
                    }

                    var scrolledY = (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop);
                    var winHeight = document.documentElement.clientHeight;
                    //console.log('winHeight: ', winHeight);
                    //console.log('scrolledY: ', scrolledY);
                    //console.log('document.clientHeight: ', document.clientHeight);
                    //console.log('document.body.clientHeight: ', document.body.clientHeight);
                    if (scrolledY + winHeight > document.body.clientHeight - 100)
                        scope.$ctrl.getNextPosts();

                });
            }
        }
    }
}

uploadingPosts.inject = ['allPostsService', '$document'];