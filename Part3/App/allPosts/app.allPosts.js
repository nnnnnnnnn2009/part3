﻿angular
    .module('app.allPosts', [])
    .constant('NUMBER_OF_POSTS_FOR_DOWNLOADING', 4)
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('all-posts', {
            url: "/all-posts",
            template: '<all-posts-component></all-posts-component>',
            data: {
                pageTitle: 'Всі пости',
            }
        });
    }])
    .controller('allPostsController', allPostsController)
    .component('allPostsComponent', {
        templateUrl: '/App/allPosts/allPosts.html',
        controller: allPostsController,
        bindings: {
        }
    })
    .directive('uploadingPosts', uploadingPosts)

