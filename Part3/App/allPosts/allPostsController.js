﻿function allPostsController(allPostsService, myPostsService, $rootScope, NUMBER_OF_POSTS_FOR_DOWNLOADING) {
    console.log('++allPostsController');
    var vm = this;
    
    vm.isLoadingPosts = false;
    vm.editingPost = {
        Title: undefined,
        Description: undefined
    }
    vm.allPostsService = allPostsService;


    if (typeof (allPostsService.allPosts) == 'undefined') {
        //console.log('allPostsService.allPosts) == undefined');
        allPostsService.getMyId().then(function (data) {
            if (typeof (data.data) == 'undefined')
                return;
            $rootScope.myId = data.data;
            vm.getNextPosts();
        });

    }


    vm.getNextPosts = function () {
        if (vm.isLoadingPosts || allPostsService.isAllPostsDownloading)
            return;

        vm.isLoadingPosts = true;
        allPostsService.getNextPosts().then(function (data) {
            console.log('%cdata next allposts: ', 'color: green;', data);
            vm.isLoadingPosts = false;
            if (typeof (data.data) == 'undefined') {
                return;
            }

            var posts = data.data;
            var len = posts.length;
            if (len < NUMBER_OF_POSTS_FOR_DOWNLOADING) {
                allPostsService.isAllPostsDownloading = true;
            }
            if (len == 0)
                return;

            for (var i = 0; i < len; i++) {
                posts[i].buttonDeleteNotAllowed = false;
                posts[i].isRequestEdit = false;
                posts[i].isOwner = $rootScope.myId == posts[i].UserId;
            }
            allPostsService.idLastDownloadingPosts = posts[len - 1].Id;
            if (typeof (allPostsService.allPosts) != 'undefined')
                allPostsService.allPosts = allPostsService.allPosts.concat(posts);
            else 
                allPostsService.allPosts = posts;
            //console.log(allPostsService.allPosts);
        }, function (response) {
            //console.log(response);
            vm.isLoadingPosts = false;
        });
    }

    vm.editPost = function (index, event) {
        console.log('in edit post');
        var post = allPostsService.allPosts[index];
        post.Title = vm.editingPost.Title;
        post.Description = vm.editingPost.Description;
        if (!validEditing(post)) 
            return;

        allPostsService.allPosts[index].isRequestEdit = true;
        myPostsService.editPost(post.Id, post).then(function (data) {
            console.log('data edit post:', data);
            modal_alert("Ваш пост успішно відредаговано!");
            post.isRequestEdit = false;
            post.Title = data.data.Title;
            post.Description = data.data.Description;
            allPostsService.allPosts[index] = post;
            if (post.isOwner && (typeof (myPostsService.myPosts) != 'undefined')) {
                var indexInMyPosts = searchByIdInArrayOfObjects(post.Id, myPostsService.myPosts);
                if (typeof(indexInMyPosts) != 'undefined') {
                    myPostsService.myPosts[indexInMyPosts].Title = post.Title;
                    myPostsService.myPosts[indexInMyPosts].Description = post.Description;
                }
            }

            var but = $(event.target).closest('.jumbotron').find('button')[0];
            but.click();
            //angular.element(but).triggerHandler('click');
        }, function (response) {
            console.log('response: ', response);
            modal_alert(response.data.Message);
            allPostsService.allPosts[index].isRequestEdit = false;
            //Невідома помилка при додаванні посту (");
        });
    }

    vm.deletePost = function (id, index) {
        console.log('in delete post');
        if (!confirm("Хочете видалити пост \"" + allPostsService.allPosts[index].Title + "\"?"))
            return;
        allPostsService.allPosts[index].buttonDeleteNotAllowed = true;
        myPostsService.deletePost(id).then(function (data) {
            console.log('data delete: ', data);
            var isOwner = allPostsService.allPosts[index].isOwner;
            modal_alert("На Ваше замовлення пост успішно видалено!");
            allPostsService.allPosts.splice(index, 1);

            if (isOwner && typeof (myPostsService.myPosts) != 'undefined') {
                indexInMyPosts = searchByIdInArrayOfObjects(id, myPostsService.myPosts);
                if (typeof (indexInMyPosts) != 'undefined')
                    myPostsService.myPosts.splice(indexInMyPosts, 1);
            }

        }, function (response) {
            console.log('data bad delete: ', response);
            modal_alert('Помилка при видаленні посту (');
            allPostsService.allPosts[index].buttonDeleteNotAllowed = false;
        });
    }

    vm.putOrCleanLike = function (index, postId, typeLike) {
        if (allPostsService.allPosts[index].ILike != null) {
            allPostsService.deleteLike(postId).then(function (data) {
                console.log('data delete like: ', data);
                allPostsService.allPosts[index].ILike = null;
            }, function (response) {
                console.log('response: ', response);
                modal_alert('Невідома помилка');
            });
        } else {
            allPostsService.putLike(postId, typeLike).then(function (data) {
                console.log(data);
                allPostsService.allPosts[index].ILike = typeLike;
            }, function (response) {
                console.log('response: ', response);
                modal_alert(response.data.Message);
            });
        }
    }

    function validEditing(post) {
        var title = post.Title.trim();
        var description = post.Description.trim();
        var error = "";

        if (title.length < 2 || title.length > 200)
            error += "Заголовок посту повинен містити від 2 до 200 символів. ";
        if (description.length < 2 || description.Length > 5000)
            error += "Текст посту повинен містити від 2 до 5000 символів. ";
                
        if (error != "") {
            modal_alert(error);
            return false;
        }
        return true;
    }


    function searchByIdInArrayOfObjects(id, arrayOfObjects) {
        var len = arrayOfObjects.length;
        var index = undefined;
        for (var i = 0; i < len; i++) {
            if (arrayOfObjects[i].Id == id) {
                index = i;
                break;
            }
        }
        return index;
    }

}

allPostsController.$inject = ['allPostsService', 'myPostsService', '$rootScope', 'NUMBER_OF_POSTS_FOR_DOWNLOADING'];