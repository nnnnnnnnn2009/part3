﻿angular
    .module('app.myPosts', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('my-posts', {
            url: "/my-posts",
            template: '<my-posts-component></my-posts-component>',
            data: {
                pageTitle: 'Мої пости',
            }
        });
    }])
    .controller('myPostsController', myPostsController)
    .component('myPostsComponent', {
        templateUrl: '/App/myPosts/myPosts.html',
        controller: myPostsController,
        bindings: {
        }
    })

