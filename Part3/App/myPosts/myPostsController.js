﻿function myPostsController(myPostsService, allPostsService) {
    console.log('++myPostsController');
    var vm = this;
    vm.isLoadingMyPosts = (typeof (myPostsService.myFLName) == 'undefined') ? true : false;
    vm.IsAddingInProcess = false;
    vm.newPost = { };
    vm.editingPost = {
        Title: undefined,
        Description: undefined
    }
    vm.myPostsService = myPostsService;

    if (typeof (myPostsService.myFLName) == 'undefined') {
        myPostsService.GetMyFirstAndLastName().then(function (data) {
            console.log('myname: ', data);
            var Owner = data.data;
            myPostsService.myFLName = Owner.FirstName + " " + Owner.LastName;

            myPostsService.getMyPosts().then(function (data) {
                console.log('data myposts: ', data);
                vm.isLoadingMyPosts = false;
                if (typeof(data.data) == 'undefined')
                    return;

                var posts = data.data;
                var len = posts.length;
                for (var i = 0; i < len; i++) {
                    posts[i].buttonDeleteNotAllowed = false;
                    posts[i].isRequestEdit = false;
                }
                myPostsService.myPosts = posts;
            });
        });
    }

    vm.addPost = function (form) {
        console.log('in add post');
        //console.log('addPostForm: ', addPostForm);
        //console.log('vm.newPost: ', vm.newPost);
        //console.log('addPostForm.classList: ', addPostForm.classList);

        if (form.$invalid)
            return false;

        vm.IsAddingInProcess = true;
        myPostsService.addPost(vm.newPost).then(function (data) {
            console.log('data add post:', data);
            vm.IsAddingInProcess = false;
            form.$setPristine();
            modal_alert("Ваш пост успішно додано!");
            vm.newPost = {};
            data.data.buttonDeleteNotAllowed = false;
            data.data.isRequestEdit = false;
            myPostsService.myPosts.unshift(data.data);
            if (typeof (allPostsService.allPosts) != 'undefined') {
                data.data.buttonDeleteNotAllowed = false;
                data.data.isRequestEdit = false;
                data.data.isOwner = true;
                data.data.FirstName = myPostsService.myFLName;
                allPostsService.allPosts.unshift(data.data);
            }

        }, function (response) {
            vm.IsAddingInProcess = false;
            console.log('response: ', response);
            modal_alert(response.data.Message);
                //Невідома помилка при додаванні посту (");
        });
    }
    
    vm.editPost = function (index, event) {
        console.log('in edit post');
        //editingPost[$index].Title
        //my_post.editingPost[$index].Description
        //isRequestEdit[$index]
        var post = myPostsService.myPosts[index];
        post.Title = vm.editingPost.Title;
        post.Description = vm.editingPost.Description;
        if (!validEditing(post)) 
            return;

        myPostsService.myPosts[index].isRequestEdit = true;
       // myPostsService.myPosts[index].editingPost.Title = post.Title;
        //myPostsService.myPosts[index].editingPost.Description = post.Description;

        myPostsService.editPost(post.Id, post).then(function (data) {
            console.log('data edit post:', data);
            modal_alert("Ваш пост успішно відредаговано!");
            post.isRequestEdit = false;
            post.Title = data.data.Title;
            post.Description = data.data.Description;
            myPostsService.myPosts[index] = post;

            if (typeof (allPostsService.allPosts) != 'undefined') {
                var len = allPostsService.allPosts.length;
                for (var i = 0; i < len; i++) {
                    if (allPostsService.allPosts[i].Id == post.Id) {
                        allPostsService.allPosts[i].Title = post.Title;
                        allPostsService.allPosts[i].Description = post.Description;
                        break;
                    }
                }
            }

            var but = $(event.target).closest('.jumbotron').find('button')[0];
            but.click();
            //angular.element(but).triggerHandler('click');
        }, function (response) {
            console.log('response: ', response);
            modal_alert(response.data.Message);
            myPostsService.myPosts[index].isRequestEdit = false;
            //Невідома помилка при додаванні посту (");
        });
    }

    vm.deletePost = function (id, index) {
        console.log('in delete post');
        if (!confirm("Хочете видалити пост \"" + myPostsService.myPosts[index].Title + "\"?"))
            return;

        myPostsService.myPosts[index].buttonDeleteNotAllowed = true;
        myPostsService.deletePost(id).then(function (data) {

            console.log('data delete: ', data);
            modal_alert("На Ваше замовлення пост успішно видалено!");
            myPostsService.myPosts.splice(index, 1);

            if (typeof (allPostsService.allPosts) != 'undefined') {
                var len = allPostsService.allPosts.length;
                var indexInAllPosts = undefined;
                for (var i = 0; i < len; i++) {
                    if (allPostsService.allPosts[i].Id == id) {
                        indexInAllPosts = i;
                        break;
                    }
                }
                if (typeof (indexInAllPosts) != 'undefined')
                    allPostsService.allPosts.splice(indexInAllPosts, 1);
            }
        }, function (response) {
            console.log('data bad delete: ', response);
            modal_alert('Помилка при видаленні посту (');
            myPostsService.myPosts[index].buttonDeleteNotAllowed = false;
        });
    }

    //vm.showSlide = editing_post_

    function validEditing(post) {
        var title = post.Title.trim();
        var description = post.Description.trim();
        var error = "";

        if (title.length < 2 || title.length > 200)
            error += "Заголовок посту повинен містити від 2 до 200 символів. ";
        if (description.length < 2 || description.Length > 5000)
            error += "Текст посту повинен містити від 2 до 5000 символів. ";
                
        if (error != "") {
            modal_alert(error);
            return false;
        }
        return true;
    }

    //console.log('vm: ', vm);

}

myPostsController.$inject = ['myPostsService', 'allPostsService'];