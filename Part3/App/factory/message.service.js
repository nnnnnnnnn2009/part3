﻿function messageService() {
    console.log('++ start messageService');

    var factory = {};

    factory.countNewMessages = 0;
    factory.messages = [];

    return factory;
}

//messageService.$inject = ['$http'];

angular
    .module('message.service', [])
    .factory('messageService', messageService)

