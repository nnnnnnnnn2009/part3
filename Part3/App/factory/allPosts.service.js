﻿function allPostsService($http, NUMBER_OF_POSTS_FOR_DOWNLOADING, $rootScope) {

    var factory = {};

    factory.allPosts = undefined;
    factory.idLastDownloadingPosts = 2147483647;
    factory.isAllPostsDownloading = false;

    factory.getMyId = function () {
        return $http.get('/Account/GetMyId');
    }

    factory.getNextPosts = function () {
        return $http.get('api/AllPosts', { params: { idLastDownloadingPosts: this.idLastDownloadingPosts, number: NUMBER_OF_POSTS_FOR_DOWNLOADING } });
    }

    factory.putLike = function (postId, likeType) {
        var config = {
            headers: { 'ConnectionId': $rootScope.connectionId }
        };
        //console.log('config in factory.putLike: ', config);
        return $http.post('api/Likes', { PostId: postId, Type: likeType }, config);
    }

    factory.deleteLike = function (id) {
        var config = {
            headers: { 'ConnectionId': $rootScope.connectionId }
        };
        //console.log('config in factory.deleteLike: ', config);
        return $http.delete('api/Likes/' + id, config);
    }

    return factory;
}

allPostsService.$inject = ['$http', 'NUMBER_OF_POSTS_FOR_DOWNLOADING', '$rootScope'];

angular
    .module('allPosts.service', [])
    .factory('allPostsService', allPostsService)

