﻿function homeService($http) {
    var factory = {};
    factory.users = undefined;

    factory.gethomeUsers = function () {
        return $http.get('/Home/GetUsers');
    }

    factory.signed = function (idUser, index) {
        return $http.post('/Home/Signed', { idUser: idUser } );
    }

    factory.outSigned = function (idFriend, index) {
        return $http.post('/Home/OutSigned', { idFriend: idFriend } );
    }

    factory.makeFriends = function (idFriend, index) {
        return $http.post('/Home/MakeFriends', { idFriend: idFriend } );
    }

    factory.deleteFriend = function (idFriend, index) {
        return $http.post('/Home/DeleteFriend', { idFriend: idFriend } );
    }
    return factory;
}

homeService.$inject = ['$http'];

angular
    .module('home.service', [])
    .factory('homeService', homeService)

