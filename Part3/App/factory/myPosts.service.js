﻿function myPostsService($http) {
    var factory = {};

    factory.myPosts = undefined;
    factory.myFLName = undefined;

    factory.GetMyFirstAndLastName = function () {
        return $http.get('/Account/GetMyFirstAndLastName');
    }

    factory.getMyPosts = function () {
        return $http.get('api/MyPosts');
    }

    factory.addPost = function (newPost) {
        return $http.post('api/MyPosts', newPost);
    }

    factory.deletePost = function (id) {
        return $http.delete('api/MyPosts/' + id);
    }

    factory.editPost = function (id, post) {
        return $http.put('api/MyPosts/' + id, post);
    }

    return factory;
}

myPostsService.$inject = ['$http'];

angular
    .module('myPosts.service', [])
    .factory('myPostsService', myPostsService)

