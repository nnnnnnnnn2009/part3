﻿function realtimeService($rootScope) {
    console.log('++start realtimeService');
    var likeHub = $.connection.likeHub;

    
   /* return {

        on: function (eventName, func) {
            likeHub.client[eventName] = func;
        },

        of: function (eventName) {
            likeHub.client[eventName] = undefined;
        },

        invoke: function (methodName, args) {
            $.connection.hub.start().done(function () {
                //console.log('arguments: ', arguments);
                //console.log('this: ', this);
                console.log('likeHub.server: ', likeHub.server);
                console.log('likeHub.client: ', likeHub.client);
                //console.log('args: ', args);
                if (args) likeHub.server[methodName](args);
                else likeHub.server[methodName]();
            })
        }
    }*/

    return {
        start: function() {
            $.connection.hub.start()
                .done(function () {
                    console.log('Connected to Realtime hub !!!');
                    $rootScope.connectionId = $.connection.hub.id;
                    console.log('$rootScope.connectionId: ', $rootScope.connectionId);
                })
                .fail(function () { console.log('Could not Connect!'); });
        },

        on: function (eventName, callback) {
            likeHub.on(eventName, function (result) {
                $rootScope.$apply(function () {
                    if (callback) {
                        //console.log('result: ', result);
                        callback(result);
                    }
                });
            });
        },

        off: function (eventName, callback) {
            likeHub.off(eventName)//, function (result) { 
            // console.log('done - off'); 
            //$rootScope.$apply(function () { 
            // if (callback) { 
            // callback(result); 
            // } 
            //}); 
            //}); 
        },

        invoke: function (methodName, args, callback) {
            $.connection.hub.start().done(function () {
                if (args) likeHub.invoke(methodName, args);
                else likeHub.invoke(methodName)
                .done(function (result) {
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback(result);
                        }
                    });
                });
            });
        }
    };
    
};

realtimeService.$inject = ['$rootScope'];

angular
    .module('realtime.service', [])
    .factory('realtimeService', realtimeService)