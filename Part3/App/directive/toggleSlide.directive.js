﻿function toggleSlide() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            //console.log('directive element: ', element);
            //console.log('children element: ', element.children());
            var popup = element.find(".editing-popup");
            var button_edit = element.find(".float-right").children()[0];
            var button_cancel = popup.children("button")[1];
            var button_save = popup.children("button")[0];
            var editDescription = popup.find("textarea");
            var editTitle = popup.find("input");
            //console.log('button_edit: ', button_edit);
            //console.log('popup: ', popup);
            //console.log('button_cancel: ', button_cancel);
            //console.log('description: ', editDescription);
            //console.log('title: ', editTitle);

            angular.element(button_edit).on('click', function () {
                console.log('click in directive');
                var display = popup.css('display');
                popup.slideToggle(1000);
                //if ($(window).scrollTop() > element.offset().top)
                if (display == 'none')
                    editTitle.focus();
                else 
                    window.scrollTo(0, element.offset().top - 60);
                //scope.$apply();
            });
            

            angular.element(button_cancel).on('click', function () {
                console.log('cancel in directive');
                popup.slideUp(1000);
                console.log('element offset: ', element.offset());
                window.scrollTo(0, element.offset().top - 60);
                var title = element.find("h2");
                editTitle.val(title.text());
                editDescription.val(title.next().text());
            });

            angular.element(button_save).on('mousedown', function () {
                console.log('mousedown scope: ', scope);
                scope.$parent.$ctrl.editingPost.Title = editTitle.val();
                scope.$parent.$ctrl.editingPost.Description = editDescription.val();
            });

        }
    };
}

//toggleSlide.inject = ['myPostsService'];