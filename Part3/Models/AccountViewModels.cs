﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Part3.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberBrowser { get; set; }
        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        public string Email { get; set; }
    }

    //is
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public List<string> ModelErrors { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "E-mail не може бути порожнім.")]
        public string Email { get; set; }

        [StringLength(60, ErrorMessage = "Ім'я повинне містити від {2} до {1} символів.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [StringLength(60, ErrorMessage = "Прізвище повинне містити від {2} до {1} символів.", MinimumLength = 2)]
        public string LastName { get; set; }

        [StringLength(60, ErrorMessage = "Пароль повинен містити від {2} до {1} символів.", MinimumLength = 6)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Пароль і його підтверждення не співпадають.")]
        public string ConfirmPassword { get; set; }

        public List<string> ModelErrors { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Пароль повинен містити не менше {2} символів.", MinimumLength = 6)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Пароль і його підтвердження не співпадають.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        public string Email { get; set; }
        public List<string> ModelErrors { get; set; }
    }
}
