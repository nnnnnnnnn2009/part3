﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Part3.Models
{
    public class Like
    {
        public int Id { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public bool Type { get; set; }
    }
}