﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Part3.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date{ get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public ICollection<Like> Likes { get; set; }
        public Post()
        {
            Likes = new List<Like>();
        }
    }
}