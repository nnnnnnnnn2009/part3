﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Part3.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date{ get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CountLikesUp { get; set; }
        public int CountLikesDown { get; set; }
        public bool? ILike { get; set; }

        public PostViewModel(Post post, string myId)
        {
            Id = post.Id;
            Title = post.Title;
            Description = post.Description;
            Date = post.Date;
            UserId = post.UserId;
            FirstName = post.User.FirstName;
            LastName = post.User.LastName;
            CountLikesUp = post.Likes.Count(l => l.Type); 
            CountLikesDown = post.Likes.Count() - CountLikesUp; 

            bool? iLike = null;
            if (post.UserId != myId)
            {
                var myLike = post.Likes.FirstOrDefault(l => l.UserId == myId);
                if (myLike != null)
                    iLike = myLike.Type;
            }
            ILike = iLike;
            if (iLike.HasValue)
                if (iLike.Value) CountLikesUp -= 1;
                else CountLikesDown -= 1;
        }

    }
}