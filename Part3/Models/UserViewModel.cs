﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Part3.Models
{
    public class UserViewModel
    {
        public int? IdFriend { get; set; }
        public string IdUser { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsFriends { get; set; }
        public bool IsISigned { get; set; }
        public bool IsSignedMe { get; set; }

        public UserViewModel(ApplicationUser user)
        {
            IdUser = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            IsFriends = false;
            IsISigned = false;
            IsSignedMe = false;
        }

    }
}