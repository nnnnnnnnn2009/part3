﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Part3.Models
{
    public class Friend
    {
        public int Id { get; set; }
        public string IdUser1 { get; set; }
        public string IdUser2 { get; set; }
        public bool IsFriends { get; set; }
        public bool IsSigned1To2 { get; set; }
        public bool IsSigned2To1 { get; set; }
    }
}