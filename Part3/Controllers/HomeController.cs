﻿using Microsoft.AspNet.Identity;
using Part3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Part3.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            var getingUsers = new List<UserViewModel>();
            var myId = User.Identity.GetUserId();
            var usersAll = db.Users.ToList();
            var user = usersAll.Find(u => u.Id == myId);
            usersAll.Remove(user);
            foreach (var ua in usersAll)
            {
                getingUsers.Add(new UserViewModel(ua));
            }
            db.Friends.Add(new Friend { IdUser1 = myId, IdUser2 = myId, IsFriends = false, IsSigned1To2 = false, IsSigned2To1 = false });
            var friends = db.Friends.Where(u => u.IdUser1 == myId || u.IdUser2 == myId).ToList();
            foreach (var f in friends)
            {
                if (f.IdUser1 == myId)
                {
                    var gu = getingUsers.Find(u => u.IdUser == f.IdUser2);
                    gu.IsFriends = f.IsFriends;
                    gu.IsISigned = f.IsSigned1To2;
                    gu.IsSignedMe = f.IsSigned2To1;
                    gu.IdFriend = f.Id;
                }
                else
                {
                    var gu = getingUsers.Find(u => u.IdUser == f.IdUser1);
                    gu.IsFriends = f.IsFriends;
                    gu.IsISigned = f.IsSigned2To1;
                    gu.IsSignedMe = f.IsSigned1To2;
                    gu.IdFriend = f.Id;
                }
            }
            return Json(new { success = true, users = getingUsers }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Signed(string idUser)
        {
            var myId = User.Identity.GetUserId();
            var friend = db.Friends.Where(u => u.IdUser1 == myId && u.IdUser2 == idUser).FirstOrDefault();
            if (friend == null)
                friend = db.Friends.Where(u => u.IdUser1 == idUser && u.IdUser2 == myId).FirstOrDefault();
            if (friend == null)
            {
                friend = new Friend { IdUser1 = myId, IdUser2 = idUser, IsFriends = false, IsSigned1To2 = true, IsSigned2To1 = false };
                db.Entry(friend).State = EntityState.Added;
                db.SaveChanges();
                var idFriend = friend.Id;
                return Json(new { success = true, idFriend = idFriend }, JsonRequestBehavior.DenyGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult OutSigned(int idFriend)
        {
            bool success = false;
            var myId = User.Identity.GetUserId();
            var friend = db.Friends.Find(idFriend);
            if (friend != null)
            {
                if (!friend.IsFriends)
                    if ((friend.IdUser1 == myId && friend.IsSigned1To2) ||
                        (friend.IdUser2 == myId && friend.IsSigned2To1))
                    {
                        db.Friends.Remove(friend);
                        db.SaveChanges();
                        success = true;
                    }
            }
            return Json(new { success = success }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult MakeFriends(int idFriend)
        {
            bool success = false;
            var myId = User.Identity.GetUserId();
            var friend = db.Friends.Find(idFriend);
            if (friend != null)
            {
                if (!friend.IsFriends)
                    if ((friend.IdUser1 == myId && friend.IsSigned2To1) ||
                    (friend.IdUser2 == myId && friend.IsSigned1To2))
                    {
                        friend.IsFriends = true;
                        db.SaveChanges();
                        success = true;
                    }
            }
            return Json(new { success = success }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult DeleteFriend(int idFriend)
        {
            bool success = false;
            var myId = User.Identity.GetUserId();
            var friend = db.Friends.Find(idFriend);
            if (friend != null)
            {
                if (friend.IsFriends && (friend.IdUser1 == myId || friend.IdUser2 == myId))
                {
                    friend.IsFriends = false;
                    db.SaveChanges();
                    success = true;
                }
            }

            return Json(new { success = success }, JsonRequestBehavior.DenyGet);
        }


    }
}