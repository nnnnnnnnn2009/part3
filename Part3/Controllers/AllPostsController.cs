﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Part3.Models;
using Microsoft.AspNet.Identity;

namespace Part3.Controllers
{
    [Authorize]
    public class AllPostsController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/AllPosts/id/number
        public List<PostViewModel> GetPosts(int idLastDownloadingPosts, int number)
        {
            var posts = db.Posts.Where(p => p.Id < idLastDownloadingPosts).Include(pl => pl.Likes).Include(p => p.User).OrderByDescending(p => p.Id).Take(number).ToList();
            //var postincl = db.Posts.Include(pin => pin.Likes).ToList();
            var postsView = new List<PostViewModel>();
            string myId = User.Identity.GetUserId();
            foreach (var post in posts)
            {
                postsView.Add(new PostViewModel(post, myId));
            }
            return postsView;
        }

    }
}