﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Part3.Models;
using Microsoft.AspNet.Identity;

namespace Part3.Controllers
{
    [Authorize]
    public class MyPostsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/MyPosts
        public IQueryable<Post> GetPosts()
        {
            var myId = User.Identity.GetUserId();
            return db.Posts.Where(p => p.UserId == myId).OrderByDescending(p => p.Id);
        }

        // GET: api/MyPosts/5
        /*[ResponseType(typeof(Post))]
        public IHttpActionResult GetPost(int id)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            return Ok(post);
        }*/

        // PUT: api/MyPosts/5
        [ResponseType(typeof(Post))]
        public IHttpActionResult PutPost(int id, Post post)
        {
            if (id != post.Id)
                return BadRequest();

            if (post.UserId != User.Identity.GetUserId())
                return BadRequest("Не маєте прав для редагування цього посту");

            string modelError = ValidationPost(post);
            if (modelError != "")
                return BadRequest(modelError);
            
            db.Entry(post).State = EntityState.Modified;
            db.SaveChanges();
            return Ok(post);
        }

        // POST: api/MyPosts
        [ResponseType(typeof(Post))]
        public IHttpActionResult PostPost(Post post)
        {
            string modelError = ValidationPost(post);
            if (modelError != "")
            { 
                return BadRequest(modelError);
            }

            post.UserId = User.Identity.GetUserId();
            post.Date = DateTime.Now;

            db.Posts.Add(post);
            db.SaveChanges();

            return Created("", post);

            //return CreatedAtRoute( ("DefaultApi", new { id = post.Id }, post);
        }

        // DELETE: api/MyPosts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult DeletePost(int id)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            var myId = User.Identity.GetUserId();
            if (post.UserId != myId)
            {
                return BadRequest();
            }

            db.Posts.Remove(post);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PostExists(int id)
        {
            return db.Posts.Count(e => e.Id == id) > 0;
        }

        private string ValidationPost(Post post)
        {
            var modelError = "";
            if (post.Title == null)
            {
                post.Title = post.Title.Trim();
                if (post.Title.Length < 2 || post.Title.Length > 200)
                    modelError += "Заголовок посту повинен містити від 2 до 200 символів. ";
            }
            if (post.Description == null)
            {
                post.Description = post.Description.Trim();
                if (post.Description.Length < 2 || post.Description.Length > 5000)
                    modelError += "Текст посту повинен містити від 2 до 5000 символів. ";
            }
            return modelError;
        }

    }
}