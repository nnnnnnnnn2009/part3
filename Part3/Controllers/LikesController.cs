﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Part3.Models;
using Part3.Hubs;
using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.SignalR;

namespace Part3.Controllers
{
    [System.Web.Http.Authorize]
    public class LikesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/Likes
        [ResponseType(typeof(Like))]
        public IHttpActionResult PostLike(Like like)
        {
            if (!ModelState.IsValid)
                return BadRequest("Некоректні дані.");

            var myName = User.Identity.Name;
            var user = db.Users.FirstOrDefault(u => u.UserName == myName);

            var post = db.Posts.Find(like.PostId);
            if (post.UserId == user.Id)
            return BadRequest("Свій пост неможливо оцінити.");

            if (db.Likes.Any(l => l.PostId == like.PostId && l.UserId == user.Id))
            return BadRequest("Ви вже дали оцінку посту \"" + post.Title + "\".");

            like.UserId = user.Id;
            db.Likes.Add(like);
            db.SaveChanges();

            var connectionId = Request.Headers.GetValues("ConnectionId").FirstOrDefault();
            SendMessagePutLike(post, user, like.Type, connectionId);
            like.Post = null;
            like.User = null;
            return Created("", like);
        }

        // DELETE: api/Likes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteLike(int id)
        {
            // id - post id
            var myName = User.Identity.Name;
            var user = db.Users.FirstOrDefault(u => u.UserName == myName);

            Like like = db.Likes.FirstOrDefault(l => l.PostId == id && l.UserId == user.Id);
            if (like == null)
            {
                return BadRequest();
            }

            var likeTypeOld = like.Type;
            db.Likes.Remove(like);
            db.SaveChanges();

            var post = db.Posts.Find(id);
            var connectionId = Request.Headers.GetValues("ConnectionId").FirstOrDefault();
            SendMessageCleanLike(post, user, null, likeTypeOld, connectionId);

            return StatusCode(HttpStatusCode.NoContent);
        }

        private void SendMessagePutLike(Post post, ApplicationUser user, bool? likeType, string connectionId)
        {
            var context = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<LikeHub>();
            var obj = new MessageClient { PostId = post.Id, PostTitle = post.Title, UserName = user.FirstName + " " + user.LastName, LikeType = likeType, IsNew = true };
            context.Clients.AllExcept(connectionId).takeLike(obj); 
        }

        private void SendMessageCleanLike(Post post, ApplicationUser user, bool? likeType, bool? likeTypeOld, string connectionId)
        {
            var context = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<LikeHub>();
            var obj = new MessageClient { PostId = post.Id, PostTitle = post.Title, UserName = user.FirstName + " " + user.LastName, LikeType = likeType, LikeTypeOld = likeTypeOld, IsNew = true };
            context.Clients.AllExcept(connectionId).cleanLike(obj);
        }

    }
}